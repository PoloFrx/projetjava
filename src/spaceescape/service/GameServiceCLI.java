package spaceescape.service;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import spaceescape.exception.BadParseException;
import spaceescape.model.Player;
import spaceescape.model.boardmap.BoardMap;
import spaceescape.model.boardmap.Coordinate;
import spaceescape.model.character.Character;

public class GameServiceCLI implements GameService {



    @Override
    public void displayPlayerMap(Player player, BoardMap map){
        displayColumn(map);
        for (int y = 0 ; y < map.getHeight() ; y++){
            displayLine(y);
            for(int x = 0 ; x < map.getWidth() ; x++){
                if(player.getCharacter().getCoordinate().equals(new Coordinate(x,y))){
                    System.out.print("X  ");
                } else {
                    System.out.print(map.getMap().get(new Coordinate(x,y)));
                    System.out.print("  ");
                }
            }
            System.out.println();

        }
    }

    @Override
    public Coordinate askPlayerWhereMove(Player player, List<Coordinate> possibleMove) {
        Scanner entry;
        Coordinate choosingMove = null;
        do{
            System.out.println("Where do you want to displacement? you are on: " + player.getCharacter().getCoordinate());
            System.out.println(possibleMove);
            entry = new Scanner(System.in);
            try{
                choosingMove = parseToCoordinate(entry.next());
            } catch (BadParseException e){
                System.out.println("Don't forget ':' between coordinate.");
            } catch (NumberFormatException e){
                System.out.println("Only numbers please.");
            }
        } while (choosingMove == null || !possibleMove.contains(choosingMove));
        return choosingMove;
    }

    @Override
    public Coordinate askPlayerWhereMakeNoise(int mapHeight, int mapWidth) {
        Scanner entry;
        Coordinate choosingRoom = null;
        do{
            System.out.println("Pick a room where make noise (all map):");
            entry = new Scanner(System.in);
            try{
                choosingRoom = parseToCoordinate(entry.next());
            } catch (BadParseException e){
                System.out.println("Don't forget ':' between coordinate.");
            } catch (NumberFormatException e){
                System.out.println("Only numbers please.");
            }
        } while (choosingRoom == null || choosingRoom.getX() < 0 || choosingRoom.getX() >= mapWidth || choosingRoom.getX() < 0 || choosingRoom.getY() >= mapWidth);
        return choosingRoom;
    }
    
    @Override
    public String askPlayerSurname(int numberPlayer) {
    	Scanner entry;
    	String surname="";
    	do {
    		entry=new Scanner(System.in);
    		System.out.println("Enter surname for player "+numberPlayer+": ");
    		surname=entry.next();
    	} while(surname.equals(""));
    	return surname;
    }
    
    @Override
    public int askHowManyPlayers() {
    	Scanner entry;
    	int numberOfPlayers=0;
    	do {
    		System.out.println("How many players will play? ");
    		entry=new Scanner(System.in);
    		try {
    			numberOfPlayers=Integer.parseInt(entry.next());
    			if(numberOfPlayers<2 || numberOfPlayers>4) {
    				throw new NumberFormatException();
    			}
    		} catch(NumberFormatException e) {
    			System.out.println("Enter a number between 2 and 4 please.");
    		}
    	} while(numberOfPlayers<2 || numberOfPlayers>4);
    	return numberOfPlayers;
    }
    
    @Override
    public void beginOfTheTurn(String surname) {
    	Scanner entry=new Scanner(System.in);
    	System.out.println("It's "+surname+"'s turn! Press Enter to begin...");
    	entry.nextLine();
    }
    
    @Override
    public int MoveOrMoveAttack() {
    	int choice;
    	boolean boucle=true;
    	do {
	    	Scanner entry=new Scanner(System.in);
	    	System.out.println("To move, enter 1, to move and then attack, enter 2: ");
	    	try {
	    		choice=Integer.parseInt(entry.nextLine());
	    		
	    		if(choice>2 || choice<1) {
	    			throw new NumberFormatException();
	    		}
	    		return choice;
	    	} catch(NumberFormatException e) {
	    		System.out.println("Entrez soit 1, soit 2.");
	    	}
    	} while(boucle);
    	return 0;
    }
    
    @Override
    public void SwitchError(String place) {
    	System.out.println("There was an error in "+place+"'s switch.");
    	System.exit(0);
    }
    
    @Override
    public void failCapsule() {
    	System.out.println("The launch of the capsule failed. You made some noise.");
    }
    
    @Override
    public void hasWin() {
    	System.out.println("The capsule was launched. You win.");
    }
    
    @Override
    public void tellWhatHappened(List<String> messages) {
    	for(String message : messages) {
    		System.out.println(message);
    	}
    }
    
    @Override
    public void displayCharacterType(String characterType) {
    	System.out.println("You are: "+characterType);
    }
    
    @Override
    public void alienDefeat() {
    	System.out.println("Aliens have lost!");
    }
    
    @Override
    public void alienVictory() {
    	System.out.println("Aliens have completely win!");
    }
    
    @Override
    public void endOfGame() {
    	System.out.println("This is the end of the game...");
    }

    /////////////////////
     // Private Section //
    /////////////////////

    private void displayColumn(BoardMap map){
        System.out.print("   ");
        for (int i = 0 ; i < map.getWidth() ; i++){
            if(i < 10){
                System.out.print(i  + "  ");

            } else {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    private void displayLine(int line){
        if(line < 10){
            System.out.print(line + "  ");

        } else {
            System.out.print(line + " ");
        }
    }

    private static Coordinate parseToCoordinate (String coordinateString) throws BadParseException {
        String[] parse = coordinateString.split(":");
        if (parse.length != 2 ){
            throw new BadParseException();
        } else {
            return new Coordinate(Integer.valueOf(parse[0]), Integer.valueOf(parse[1]));
        }
    }



}
