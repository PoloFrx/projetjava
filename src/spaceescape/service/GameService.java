package spaceescape.service;

import java.util.List;

import spaceescape.model.Player;
import spaceescape.model.boardmap.BoardMap;
import spaceescape.model.boardmap.Coordinate;

public interface GameService {

    void displayPlayerMap(Player player, BoardMap map);
    Coordinate askPlayerWhereMove(Player player, List<Coordinate> possibleMove);
    Coordinate askPlayerWhereMakeNoise(int mapHeight, int mapWidth);
    String askPlayerSurname(int numberPlayer);
    int askHowManyPlayers();
    void beginOfTheTurn(String surname);
    int MoveOrMoveAttack();
    void SwitchError(String place);
    void failCapsule();
    void hasWin();
    void tellWhatHappened(List<String> messages);
    void displayCharacterType(String characterType);
    void alienDefeat();
    void alienVictory();
    void endOfGame();
}
