package spaceescape.model.boardmap;

import java.util.LinkedList;

public class Room {
	private RoomType type;
	private LinkedList<Placeable> placeables;
	
	public Room(RoomType type) {
		this.type = type;
		this.placeables = null;
	}

	public RoomType getType() {
		return type;
	}

	public void setType(RoomType type) {
		this.type = type;
	}

	public LinkedList<Placeable> getPlaceables() {
		return placeables;
	}

	public void setPlaceables(LinkedList<Placeable> placeables) {
		this.placeables = placeables;
	}

	@Override
	public String toString() {
		return type.getCharacterRoomType();
	}
}
