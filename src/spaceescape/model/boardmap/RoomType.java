package spaceescape.model.boardmap;

public enum RoomType {
	CONDEMNED("*"), SAFE("_"), UNSAFE("-"), CAPSULE("O"), USEDCAPSULE("H"), DEFICIENTCAPSULE("#"), ALIEN_SPAWN("A"), MARINE_SPAWN("M");
	
	private String characterRoomType;
	
	private RoomType(String characterTypeRoom) {
		this.characterRoomType=characterTypeRoom;
	}

	public String getCharacterRoomType() {
		return characterRoomType;
	}

	public void setCharacterRoomType(String characterTypeRoom) {
		this.characterRoomType = characterTypeRoom;
	}
	
	
}
