package spaceescape.model.character;

import spaceescape.model.boardmap.*;

public abstract class Character implements Placeable {
	protected int movement;
	protected boolean canAttack;
	protected boolean alive;
	protected final String name;
	protected boolean escape;
	protected Coordinate coordinate;
	
	protected Character(int movement, String name, boolean canAttack) {
		this.movement = movement;
		this.alive = true;
		this.name = name;
		this.escape = false;
		this.canAttack=canAttack;
		this.coordinate=null;
	}

	public int getMovement() {
		return movement;
	}

	public void setMovement(int movement) {
		this.movement = movement;
	}

	public boolean isCanAttack() {
		return canAttack;
	}

	public void setCanAttack(boolean canAttack) {
		this.canAttack = canAttack;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public String getName() {
		return name;
	}

	public boolean isEscape() {
		return escape;
	}

	public void setEscape(boolean escape) {
		this.escape = escape;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	
	
}
