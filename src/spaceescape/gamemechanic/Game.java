﻿package spaceescape.gamemechanic;

import spaceescape.model.boardmap.BoardMap;
import spaceescape.model.boardmap.Coordinate;
import spaceescape.model.boardmap.RoomType;
import spaceescape.model.marine.Engineer;
import spaceescape.model.marine.Soldier;
import spaceescape.service.GameService;
import spaceescape.model.Player;
import spaceescape.model.alien.Lurker;
import spaceescape.model.alien.Praetorian;
import spaceescape.model.character.Character;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private String name;
    private BoardMap map;
    private List<Player> players = new ArrayList<>();
    private GameService gameService;
    private List<String> messages[]= new ArrayList[4];
    //Variable pour voir s'il y a déjà eu une capsule de raté ou non
    private boolean failedCapsule;

    public Game(String name, BoardMap map, List<Player> activePlayers, GameService gameService) {
        this.name = name;
        this.map = map;
        this.players.addAll(activePlayers);
        this.gameService = gameService;
        this.failedCapsule=false;
        for(int i=0; i<4;i++) {
        	messages[i]=new ArrayList<String>();
        }
    }


    /**
     * Assign a unique Character for each player randomly.
     */
    private void initGame() {
    	List<Integer> numbersAlreadyTaken= new ArrayList<Integer>(); //On aura une liste qui contiendra les nombres déjà générés et donc qu'il faut exclure
    	int number; //contiendra le nombre généré aléatoirement
    	//
    	
    	for(int i=0;i<players.size();i++) {
    		do { //On génère un nombre aléatoire, qui décidera du personnage du joueur, tant que celui-ci est pris 
    			number=(int)(Math.random()*4);
    		} while(numbersAlreadyTaken.contains(number));
    		numbersAlreadyTaken.add(number); //On ajoute le nombre à la liste des nombres à exclure
    		
    		//On fait en sorte que s'il y a 3 players, les aliens sont majoritaires puis s'il y a 2 players, qu'ils soient de type différent
    		if(players.size()==3 || players.size()==2) {
    			//Si on a déjà 1 humain, on enlève l'autre des possibilités
    			if(numbersAlreadyTaken.contains(2)) {
    				numbersAlreadyTaken.add(3);
    			} else if(numbersAlreadyTaken.add(3)) {
    				numbersAlreadyTaken.add(2);
    			}
    		} 
    		if(players.size()==2) {
    			if(numbersAlreadyTaken.contains(1)) {
    				numbersAlreadyTaken.add(2);
    			} else if(numbersAlreadyTaken.add(2)) {
    				numbersAlreadyTaken.add(1);
    			}
    		}
    		
    		//On assigne un personnage au joueur selon le nombre généré aléatoirement
    		switch(number) {
    		case 0:
    			Lurker lurker=new Lurker();
    			lurker.setCoordinate(map.getAlienSpawn());
    			players.get(i).setCharacter(lurker);
    			break;
    		case 1:
    			Praetorian praetorian =new Praetorian();
    			praetorian.setCoordinate(map.getAlienSpawn());
    			players.get(i).setCharacter(praetorian);
    			break;
    		case 2:
    			Soldier soldier=new Soldier();
    			soldier.setCoordinate(map.getMarineSpawn());
    			players.get(i).setCharacter(soldier);
    			break;
    		case 3:
    			Engineer engineer=new Engineer();
    			engineer.setCoordinate(map.getMarineSpawn());
    			players.get(i).setCharacter(engineer);
    			break;
    		default:
    			gameService.SwitchError("initGame");
    		}
    	}
    	
    	
    }
    
    private void playerTurn(Player player, int numberPlayer) {
    	gameService.beginOfTheTurn(player.getSurname());
    	Character playerCharacter=player.getCharacter();
    	boolean hasAttacked=false;
    	
    	//On affiche la map
    	gameService.displayPlayerMap(player, map);
    	//On affiche le rôle du joueur
    	gameService.displayCharacterType(playerCharacter.getName());
    	
    	//On affiche les messages qui se sont passés depuis le dernier tour de ce player puis on vide
    	gameService.tellWhatHappened(messages[numberPlayer]);
    	messages[numberPlayer].clear();
    	
    	//On garde sa position initiale pour l'exclure après des possibilités de déplacement
    	Coordinate initialCoordinate=playerCharacter.getCoordinate();
    	
    	//Choix déplacer + attaquer ou juste déplacer (selon autorisations)
    	if(playerCharacter.isCanAttack()) {
	    	switch(gameService.MoveOrMoveAttack()) {
	    		case 1:
	    			for(int i=0;i<playerCharacter.getMovement();i++) {
	    				moveAction(player, initialCoordinate);
	    			}
	    			break;
	    		case 2:
	    			for(int i=0;i<playerCharacter.getMovement();i++) {
	    				moveAction(player, initialCoordinate);
	    			}
	    			attackAction(player, numberPlayer);
	    			hasAttacked=true;
	    			break;
	    		default:
	    			gameService.SwitchError("playerTurn");
	    	}
    	} else {
    		moveAction(player, initialCoordinate);
    	}
    	
    	
    	//Doivent se déplacer
    	
    	//action de la salle si le personnage n'a pas attaqué
    	if(!hasAttacked) {
    		roomAction(player, numberPlayer);
    	}
    }
    
    private void moveAction(Player player, Coordinate initialCoordinate) {
    	List<Coordinate> possibleMove=whereCharacterCanGo(player, initialCoordinate);
    	Coordinate coordinateToMove=gameService.askPlayerWhereMove(player, possibleMove);
    	displace(coordinateToMove, player);
    }
    
    private void attackAction(Player player, int numberPlayer) {
    	for(int i=0;i<players.size();i++) {
    		//On tue tous ceux qui sont sur la case et qui ne sont pas le joueur ou un praetorian
    		if(players.get(i).getCharacter().getCoordinate().equals(player.getCharacter().getCoordinate()) && !players.get(i).equals(player) && players.get(i).getCharacter().getName()!="Praetorian") {
    			players.get(i).getCharacter().setAlive(false);
    		}
    	}
    	addMessageForOthers(player.getCharacter().getName()+" has attacked in "+player.getCharacter().getCoordinate()+".", numberPlayer);
    }
    
    private void displace(Coordinate coordinateToMove, Player player) {
    	//On change les coordonnées du player puis on réaffiche la map
    	moveCharacterTo(coordinateToMove, player);
    	gameService.displayPlayerMap(player, map);
    }
    
    private List<Coordinate> whereCharacterCanGo(Player player, Coordinate initialCoordinate) {
    	Character playerCharacter=player.getCharacter();
    	List<Coordinate> whereHeCanGo=new ArrayList<Coordinate>();
    	int xPlayer = player.getCharacter().getCoordinate().getX();
    	int yPlayer = player.getCharacter().getCoordinate().getY();
    	
    	//On choisit les coordonnées à côté du joueur et on regarde s'il peut s'y déplacer
    	Coordinate room1=new Coordinate(xPlayer+1,yPlayer);
    	RoomType room1Type=map.getMap().get(room1).getType();
    	if(room1Type!=RoomType.ALIEN_SPAWN && room1Type!=RoomType.MARINE_SPAWN && room1Type!=RoomType.CONDEMNED && room1Type!=RoomType.DEFICIENTCAPSULE && room1Type!=RoomType.USEDCAPSULE && !room1.equals(initialCoordinate)) {
    		whereHeCanGo.add(room1);
    	}
    	
    	Coordinate room2=new Coordinate(xPlayer,yPlayer+1);
    	RoomType room2Type=map.getMap().get(room2).getType();
    	if(room2Type!=RoomType.ALIEN_SPAWN && room2Type!=RoomType.MARINE_SPAWN && room2Type!=RoomType.CONDEMNED && room2Type!=RoomType.DEFICIENTCAPSULE && room2Type!=RoomType.USEDCAPSULE && !room2.equals(initialCoordinate)) {
    		whereHeCanGo.add(room2);
    	}
    	
    	Coordinate room3=new Coordinate(xPlayer-1,yPlayer);
    	RoomType room3Type=map.getMap().get(room3).getType();
    	if(room3Type!=RoomType.ALIEN_SPAWN && room3Type!=RoomType.MARINE_SPAWN && room3Type!=RoomType.CONDEMNED && room3Type!=RoomType.DEFICIENTCAPSULE && room3Type!=RoomType.USEDCAPSULE && !room3.equals(initialCoordinate)) {
    		whereHeCanGo.add(room3);
    	}
    	
    	Coordinate room4=new Coordinate(xPlayer,yPlayer-1);
    	RoomType room4Type=map.getMap().get(room4).getType();
    	if(room4Type!=RoomType.ALIEN_SPAWN && room4Type!=RoomType.MARINE_SPAWN && room4Type!=RoomType.CONDEMNED && room4Type!=RoomType.DEFICIENTCAPSULE && room4Type!=RoomType.USEDCAPSULE && !room4.equals(initialCoordinate)) {
    		whereHeCanGo.add(room4);
    	}
    	
    	//Si c'est le lurker, il peut se déplacer sur la même case qu'avant
    	if(playerCharacter.getName()=="Lurker") {
    		whereHeCanGo.add(initialCoordinate);
    	}
    	
    	return whereHeCanGo;
    }
    
    private void moveCharacterTo(Coordinate coordinateToMove, Player player) {
    	player.getCharacter().setCoordinate(coordinateToMove);
    }
    
    private void addMessageForOthers(String message, int numberPlayer) {
    	for(int i=0;i<4;i++) {
    		if(i!=numberPlayer) {
    			messages[i].add(message);
    		}
    	}
    }
    
    private void unsafeDraw(Coordinate playerCoordinate, int numberPlayer) {
    	int chance=(int)(Math.random()*3);
    	
    	switch(chance) {
    		case 0: //silence
    			addMessageForOthers("***Silence***", numberPlayer);
    			//gameService.unsafeSilence();
    			break;
    		case 1: //malchance
    			//gameService.unsafeBadLuck(playerCoordinate);
    			addMessageForOthers("Some noise have been detected in "+playerCoordinate+".", numberPlayer);
    			break;
    		case 2: //chance
    			Coordinate falseCoordinates=gameService.askPlayerWhereMakeNoise(map.getHeight(), map.getWidth());
    			addMessageForOthers("Some noise have been detected in "+falseCoordinates+".", numberPlayer);
    		default:
    			gameService.SwitchError("unsafeDraw");
    	}
    }
    
    private void openCapsule(Player player, int numberPlayer) {
    	addMessageForOthers(player.getSurname()+" try to open the capsule in"+player.getCharacter().getCoordinate()+".", numberPlayer);
    	int result=(int)(Math.random()*5);
    	
    	if(result==4 && !failedCapsule && player.getCharacter().getName()!="Engineer") {
    		//fail
    		gameService.failCapsule();
    		failedCapsule=true;
    		map.getMap().get(player.getCharacter().getCoordinate()).setType(RoomType.DEFICIENTCAPSULE);
    	} else {
    		//réussite
    		gameService.hasWin();
    		player.setWin(true);
    		player.getCharacter().setEscape(true);
    		map.getMap().get(player.getCharacter().getCoordinate()).setType(RoomType.USEDCAPSULE);
    	}
    }
    
    private void roomAction(Player player, int numberPlayer) {
    	RoomType thisRoomType=map.getMap().get(player.getCharacter().getCoordinate()).getType();
    	if(thisRoomType==RoomType.UNSAFE) {
    		unsafeDraw(player.getCharacter().getCoordinate(), numberPlayer);
    	} else if(thisRoomType==RoomType.CAPSULE && (player.getCharacter().getName()=="Engineer" || player.getCharacter().getName()=="Soldier")) {
    		openCapsule(player, numberPlayer);
    	}
    }

    public void play() {
    	//On initialise le jeu
        this.initGame();
        
        //On déclare et initie le nombre de marines morts et de tués ainsi que le nombre total de Marines
        int numberEscapedMarines=0;
        int numberDeadMarines=0;
        int numberEscapedOrDeadMarines=0;
        int numberOfMarines=(int) Math.floor(players.size()/2);
        
        do {
        	//On fait le tour pour chaque personnage
        	for(int i=0;i<players.size();i++) {
        		Character character=players.get(i).getCharacter();
        		//Si le personnage est en vie et ne s'est pas échappé on effectue son tour
        		if(!character.isEscape() && character.isAlive()) {
        			//On lance le tour du joueur en appelant la fonction et en lui donnant le joueur actuel
        			this.playerTurn(players.get(i), i);
        			//Si le joueur est mort, on incrémente le nombre de joueurs morts
        			if(!players.get(i).getCharacter().isAlive()) {
        				numberDeadMarines++;
        			} else if(players.get(i).getCharacter().isEscape()) {
        				numberEscapedMarines++;
        			}
        		}
        	}
        	numberEscapedOrDeadMarines=numberDeadMarines+numberEscapedMarines;
        } while(numberEscapedOrDeadMarines!=numberOfMarines);
        //Tant que le nombre de marines échappés ou tués n'est pas le même que le nombre total de marines
        
        //Conditions de victoire: si tous les marines se sont échappés ou s'ils ont tous été tués
        if(numberEscapedMarines==numberOfMarines) {
        	gameService.alienDefeat();
        } else if(numberDeadMarines==numberOfMarines) {
        	gameService.alienVictory();
        }
        
        //Fin du jeu
        gameService.endOfGame();
    }

}

