package spaceescape;

import java.util.LinkedList;

import spaceescape.gamemechanic.Game;
import spaceescape.model.Player;
import spaceescape.model.boardmap.BoardMap;
import spaceescape.service.GameServiceCLI;

public class Test {

	public static void main(String[] args) {
		BoardMap map= new BoardMap();
		GameServiceCLI gameCLI=new GameServiceCLI();
		LinkedList<Player> players=new LinkedList<Player>();
		
		int NumberOfPlayers=gameCLI.askHowManyPlayers();
    	for(int i=0;i<NumberOfPlayers;i++) {
    		Player player=new Player(gameCLI.askPlayerSurname(i+1));
    		players.add(player);
    	}
		
		Game game=new Game("Jeu du Space Escape",map, players, gameCLI);
		game.play();
	}

}
